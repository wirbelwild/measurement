# Changes in Bit&Black Measurement

## 0.3.0 2020-11-27

### Changed

-   `Measurement::getInformation()` has been deprecated and replaced by `Measurement::getSummary()`.

### Added 

-   Added support for PHP 8.