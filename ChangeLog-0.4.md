# Changes in Bit&Black Measurement v0.4

## 0.4.0 2021-10-30

### Changed

- `Unit::setPercent()` has been deprecated as it is no longer needed to handle the percentage values.