# Changes in Bit&Black Measurement v1.0

## 1.0.1 2022-06-09

### Fixed

-   Fixed bug with total time.

## 1.0.0 2022-06-08

### Changed

-   `Unit::getStart()` returns a `DateTime` object now.
-   `Unit::getEnd()` returns a `DateTime` object or `null` now.