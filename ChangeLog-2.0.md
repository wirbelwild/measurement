# Changes in Bit&Black Measurement v2.0

## 2.0.0 2024-09-26

### Changed

-   PHP >= 8.2 is now required.