[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/measurement)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/measurement/v/stable)](https://packagist.org/packages/bitandblack/measurement)
[![Total Downloads](https://poser.pugx.org/bitandblack/measurement/downloads)](https://packagist.org/packages/bitandblack/measurement)
[![License](https://poser.pugx.org/bitandblack/measurement/license)](https://packagist.org/packages/bitandblack/measurement)

# Bit&Black Measurement

Simple PHP code time measurement. Using time units allows receiving information about the start time and end time of a unit and also its execution length in total seconds and in percent. 

If [angle/chrono](https://packagist.org/packages/angle/chrono) is the easiest way, this is the second easiest.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/measurement). Add it to your project by running `$ composer require bitandblack/measurement`.

## Usage 

Set up a new Measurement object once:

````php
<?php 

use BitAndBlack\Measurement\Measurement;

$measurement = new Measurement();
````

Create a Unit object for every part you want to measure, add it to the measurement object and start the measurement:

````php
<?php 

use BitAndBlack\Measurement\Unit;

$unit = new Unit('Sample Unit');

$measurement->add(
    $unit->start()
);
````

When you want to end the measurement of a unit, call `$unit->end()`.

To receive the information about all your units, call `$measurement->getSummary()`. In out example, this would get something like:

````text
Array
(
    [0] => Array
        (
            [description] => Sample Unit
            [started] => object(DateTime) // 2022-07-04 07:08:38.032900
            [ended] => object(DateTime) // 2022-07-04 07:08:40.035100
            [tookTime] => 2.0021901130676
            [tookPercent] => 100
        )
)
````

Each unit object stores its own information, that can be access by regular getter methods. So you can access them for example like that:

````php
<?php 

var_dump('Sample Unit took ' . $unit->getTime() . ' seconds.');
````

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
