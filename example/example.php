<?php

use BitAndBlack\Measurement\Measurement;
use BitAndBlack\Measurement\Unit;

require dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$measurement = new Measurement();

$unit1 = new Unit('First part');
$unit2 = new Unit('Second part');

$measurement->add(
    $unit1->start(),
    $unit2->start()
);

sleep(2);

$unit1->end();

$unit3 = new Unit('Third part');

$measurement->add(
    $unit3->start()
);

sleep(4);

$unit2->end();
$unit3->end();

var_dump($measurement->getSummary());
