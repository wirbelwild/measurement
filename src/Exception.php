<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Measurement;

class Exception extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
