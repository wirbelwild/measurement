<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Measurement\Exception;

use BitAndBlack\Measurement\Exception;

class TimeException extends Exception
{
    public function __construct()
    {
        parent::__construct('Failed creating DateTime object.');
    }
}
