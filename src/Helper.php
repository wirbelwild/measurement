<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Measurement;

use BitAndBlack\Measurement\Exception\TimeException;
use DateTime;

class Helper
{
    /**
     * @throws TimeException
     */
    public static function getCurrentTime(): DateTime
    {
        $microtime = microtime(true);
        $dateTime = DateTime::createFromFormat('U.u', (string) $microtime);

        if (false === $dateTime) {
            throw new TimeException();
        }

        return $dateTime;
    }

    /**
     * Returns the total difference between to dates in seconds.
     *
     * @return float
     */
    public static function getTotalDiffSeconds(DateTime $start, DateTime $end): float
    {
        $startMicrotime = (float) $start->format('Uu');
        $endMicrotime = (float) $end->format('Uu');

        $total = $endMicrotime - $startMicrotime;
        $total /= 1_000_000;
        return $total;
    }
}
