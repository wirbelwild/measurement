<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Measurement;

use BitAndBlack\Measurement\Exception\TimeException;
use DateTime;
use ReflectionClass;

/**
 * The Measurement class holds all units and calculates their total and percentage time values.
 */
class Measurement
{
    /**
     * @var array<int, Unit>
     */
    private array $units = [];

    /**
     * @var array<int, array{
     *     description: string,
     *     started: DateTime,
     *     ended: DateTime|null,
     *     tookTime: float,
     *     tookPercent?: float,
     * }>
     */
    private array $summary = [];
    
    private bool $hasCreatedSummary = false;

    /**
     * Adds one or multiple units to the measurement.
     *
     * @return Measurement
     */
    public function add(Unit ...$unit): self
    {
        array_push($this->units, ...$unit);
        return $this;
    }

    /**
     * Creates the summary. This method can be called manually but will also be called
     * when using the {@see Measurement::getSummary()} method.
     *
     * @return Measurement
     * @throws Exception\TimeException
     */
    public function createSummary(): self
    {
        if ($this->hasCreatedSummary) {
            return $this;
        }
        
        $this->endAll();

        $timeLatest = $timeEarliest = Helper::getCurrentTime();
        
        foreach ($this->units as $key => $unit) {
            $start = $unit->getStart();
            $end = $unit->getEnd();

            /**
             * This should never happen as we end all unit measurements before,
             * what adds a datetime object to this variable.
             */
            if (null === $end) {
                continue;
            }

            $this->summary[$key] = [
                'description' => $unit->getDescription(),
                'started' => $start,
                'ended' => $end,
                'tookTime' => $unit->getTime(),
            ];

            if ($start < $timeEarliest) {
                $timeEarliest = $start;
            }
            
            if ($end > $timeLatest) {
                $timeLatest = $end;
            }
        }

        $timeTotal = Helper::getTotalDiffSeconds($timeEarliest, $timeLatest);

        foreach ($this->summary as $key => &$unit2) {
            $percent = $unit2['tookTime'] / $timeTotal * 100;
            $percent = round($percent, 2);

            /**
             * This should never happen...
             */
            if (100 < $percent) {
                $percent = 100;
            }

            $unit2['tookPercent'] = $percent;
            
            $reflectionClass = new ReflectionClass(Unit::class);

            $reflectionProperty = $reflectionClass->getProperty('percent');
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($this->units[$key], $percent);
        }
        
        unset($unit2);
        
        $this->hasCreatedSummary = true;
        return $this;
    }

    /**
     * Ends the measurement of all units.
     *
     * @return Measurement
     * @throws Exception\TimeException
     */
    public function endAll(): self
    {
        foreach ($this->units as $unit) {
            $unit->end();
        }

        return $this;
    }

    /**
     * Creates and returns the summary.
     *
     * @return array<int, array{
     *     description: string,
     *     started: DateTime,
     *     ended: DateTime|null,
     *     tookTime: float,
     *     tookPercent?: float,
     * }>
     * @throws TimeException
     */
    public function getSummary(): array
    {
        if (!$this->hasCreatedSummary) {
            $this->createSummary();
        }
        
        return $this->summary;
    }

    /**
     * Returns a list of all units.
     *
     * @return array<int, Unit>
     */
    public function getUnits(): array
    {
        return $this->units;
    }
}
