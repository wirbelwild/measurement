<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Measurement;

use BitAndBlack\Measurement\Exception\TimeException;
use DateTime;

/**
 * The Unit class is for measuring a specific behaviour (unit).
 *
 * @package BitAndBlack\Measurement
 */
class Unit
{
    private readonly string $description;

    private DateTime $start;

    private ?DateTime $end = null;

    private float $percent = 100.0;

    private float $time;

    /**
     * Unit constructor.
     *
     * @param string|null $description A title or simple description what this unit measures.
     * @throws TimeException
     */
    public function __construct(string $description = null)
    {
        $this->description = $description ?? uniqid('unit', true);
        $this->start = Helper::getCurrentTime();
    }

    /**
     * Starts the measurement manually. The measurement starts automatically when initializing
     * a new Unit object, but can be restarted at a later point with this method.
     *
     * @return Unit
     * @throws TimeException
     */
    public function start(): self
    {
        $this->start = Helper::getCurrentTime();
        return $this;
    }

    /**
     * Ends the measurement. This method will be called automatically when
     * a summary gets created ({@see Measurement::createSummary()}).
     *
     * @return Unit
     * @throws TimeException
     */
    public function end(): self
    {
        if (null === $this->end) {
            $this->end = Helper::getCurrentTime();
        }

        $this->time = Helper::getTotalDiffSeconds($this->start, $this->end);
        return $this;
    }

    /**
     * Returns the time this unit ran.
     *
     * @return float
     */
    public function getTime(): float
    {
        return $this->time;
    }

    /**
     * Returns the description of this unit.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Returns the start time,
     *
     * @return DateTime
     */
    public function getStart(): DateTime
    {
        return $this->start;
    }

    /**
     * Returns the end time.
     *
     * @return DateTime|null
     */
    public function getEnd(): ?DateTime
    {
        return $this->end;
    }

    /**
     * Returns a percent value of the units' duration.
     *
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }
}
