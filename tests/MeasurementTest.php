<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\MeasurementTests;

use BitAndBlack\Measurement\Exception\TimeException;
use BitAndBlack\Measurement\Measurement;
use BitAndBlack\Measurement\Unit;
use PHPUnit\Framework\TestCase;

/**
 * Class MeasurementTest.
 *
 * @package BitAndBlack\MeasurementTests
 */
class MeasurementTest extends TestCase
{
    /**
     * Tests if percents can be counted.
     * @throws TimeException
     */
    public function testCanCountPercents1(): void
    {
        $measurement = new Measurement();

        $unit1 = new Unit();
        $measurement->add($unit1->start());
        sleep(1);
        $unit1->end();

        $unit2 = new Unit();
        $measurement->add($unit2->start());
        sleep(3);
        $unit2->end();
        
        $measurement->createSummary();

        self::assertSame(
            25.0,
            round($unit1->getPercent())
        );

        self::assertSame(
            75.0,
            round($unit2->getPercent())
        );
    }

    /**
     * Tests if percents can be counted.
     * @throws TimeException
     */
    public function testCanCountPercents2(): void
    {
        $measurement = new Measurement();

        $unit3 = new Unit();
        $measurement->add($unit3->start());

        $unit1 = new Unit();
        $measurement->add($unit1->start());
        sleep(1);
        $unit1->end();

        $unit2 = new Unit();
        $measurement->add($unit2->start());
        sleep(3);
        $unit2->end();

        $unit3->end();

        $measurement->createSummary();

        self::assertSame(
            25.0,
            round($unit1->getPercent())
        );

        self::assertSame(
            75.0,
            round($unit2->getPercent())
        );
        
        self::assertSame(
            100.0,
            round($unit3->getPercent(), 2)
        );
    }
}
