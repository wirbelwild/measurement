<?php

/**
 * Bit&Black Measurement.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\MeasurementTests;

use BitAndBlack\Measurement\Exception\TimeException;
use BitAndBlack\Measurement\Unit;
use PHPUnit\Framework\TestCase;

/**
 * Class UnitTest.
 */
class UnitTest extends TestCase
{
    /**
     * @throws TimeException
     */
    public function testCanMeasureTime(): void
    {
        $unit = new Unit();
        sleep(1);
        $unit->end();
        
        $time = $unit->getTime();

        self::assertSame(
            1.0,
            round($time)
        );

        $unit = new Unit();
        usleep(250);
        $unit->end();

        $time = $unit->getTime();

        self::assertLessThan(
            0.1,
            $time
        );
    }
}
